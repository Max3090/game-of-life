// ...
// ...
// ...

#ifndef BIT_UTIL_H
#define BIT_UTIL_H


#include <string>


//////////////////////////////////////////////////////////////////////////
//! \brief Classe consolidant plusieurs fonctions 
//! utilitaires liées à la manipulation des bits.
//! 
//! Les fonctions sont statiques et ne requièrent pas d'instance de la 
//! classe.
//////////////////////////////////////////////////////////////////////////
class BitUtil
{
public:
	//////////////////////////////////////////////////////////////////////////
	//! \brief Le constructeur est retiré puisqu'aucune instance de 
	//! cette classe n'est attendue.
	//////////////////////////////////////////////////////////////////////////
	BitUtil() = delete;
	//////////////////////////////////////////////////////////////////////////
	//! \brief Le destructeur est retiré puisqu'aucune instance de 
	//! cette classe n'est attendue.
	//////////////////////////////////////////////////////////////////////////
	~BitUtil() = delete;

	//////////////////////////////////////////////////////////////////////////
	//! \brief Retourne si le bit demandé est à 1.
	//!
	//! \param[in] number est l'entier à interroger
	//! \param[in] digit est l'index du bit à interroger
	//! \return vrai si le bit spécifié est à 1, faux sinon
	//////////////////////////////////////////////////////////////////////////
	static bool isBitSet(size_t number, size_t digit);

	// Effectue la conversion d'un nombre entier vers 
	// un text représentant le nombre binaire.
	//////////////////////////////////////////////////////////////////////////
	//! \brief Effectue la conversion d'un nombre entier en base 2 vers une 
	//! chaîne de caractères.
	//!
	//! \param[in] value le nombre à convertir
	//! \param[in] nFirstDigit est le nombre de bit à convertir
	//! \return la chaîne de caractère issue de la conversion
	//////////////////////////////////////////////////////////////////////////
	static std::string int2bin(int value, size_t nFirstDigit);
};

#endif // BIT_UTIL_H