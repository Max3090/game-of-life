// ...
// ...
// ...

#ifndef MATH_UTIL_H
#define MATH_UTIL_H

//////////////////////////////////////////////////////////////////////////
//! \brief Classe consolidant plusieurs fonctions 
//! utilitaires liées aux mathématiques.
//! 
//! Les fonctions sont statiques et ne requièrent pas d'instance de la 
//! classe.
//////////////////////////////////////////////////////////////////////////
class MathUtil
{
public:
	//////////////////////////////////////////////////////////////////////////
	//! \brief Le constructeur est retiré puisqu'aucune instance de 
	//! cette classe n'est attendue.
	//////////////////////////////////////////////////////////////////////////
	MathUtil() = delete;
	//////////////////////////////////////////////////////////////////////////
	//! \brief Le destructeur est retiré puisqu'aucune instance de 
	//! cette classe n'est attendue.
	//////////////////////////////////////////////////////////////////////////
	~MathUtil() = delete;

	//////////////////////////////////////////////////////////////////////////
	//! \brief Fonction calculant la position d'une valeur dans un 
	//! interval donné.
	//!
	//! Soit l'interval i défini par [0, `width`], `warp` calcul la position 
	//! de `value` dans `i` considérant un univers circulaire.
	//!
	//! \param value la valeur à déterminer selon `i`
	//! \param width la borne supérieure de l'interval
	//! \return la valeur de `value` selon `i`
	//////////////////////////////////////////////////////////////////////////
	static int warp(int value, int width);

	//static double warp(double value, double begin, double end);
	//static double warp(double value, double width);
	//static int warp(int value, int begin, int end);
	//static int warp(int value, int width);
	//static size_t warp(size_t value, size_t begin, size_t end);
	//static size_t warp(size_t value, size_t width);
};


#endif // MATH_UTIL_H
