#include "Point.h"

#include <cmath>


// ATTENTION, cette partie est souvent oubli�e par les programmeurs novices en C++
std::string Point::mPrefix("(");            //< impl�mentation de la variable statique mPrefix    - la variable est aussi construite avec une valeur par d�faut (utilisation du constructeur adapt�e de la classe std::string)
std::string Point::mSeparator(", ");        //< impl�mentation de la variable statique mSeparator - la variable est aussi construite avec une valeur par d�faut (utilisation du constructeur adapt�e de la classe std::string)
std::string Point::mSuffix(")");            //< impl�mentation de la variable statique mSuffix    - la variable est aussi construite avec une valeur par d�faut (utilisation du constructeur adapt�e de la classe std::string)


Point::Point(int x, int y)
    : mX{ x }, mY{ y }                      //< liste d'initialisation membre : on initialise les variables mX et mY par les variables x et y
{
}

Point::Point(int value)
    : Point(value, value)                   //< liste d'initialisation membre : on d�l�gue la construction au contructeur � 2 param�tres
{
}

Point::Point()                              
    : Point(0)                              //< liste d'initialisation membre : on d�l�gue la construction au contructeur � 1 param�tre
{
}

Point::~Point()
{
}

int Point::x() const
{
    return mX;
}

int Point::y() const
{
    return mY;
}

void Point::setX(int x)
{
    mX = x;
}

void Point::setY(int y)
{
    mY = y;
}

void Point::set(int x, int y)
{
    setX(x); // DRY
    setY(y); // DRY
}

int Point::distance2(Point const & p)
{
    return (p.mX - mX) * (p.mX - mX) + (p.mY - mY) * (p.mY - mY);
}

double Point::distance(Point const & p)
{
    return std::sqrt(distance2(p)); // DRY
}

std::string Point::toString() const
{
    return mPrefix + std::to_string(mX) + mSeparator + std::to_string(mY) + mSuffix;
}

std::string Point::prefix()
{
    return mPrefix;
}

std::string Point::separator()
{
    return mSeparator;
}

std::string Point::suffix()
{
    return mSuffix;
}

void Point::setFormat(std::string const & prefix, std::string const & separator, std::string const & suffix)
{
    mPrefix = prefix;
    mSeparator = separator;
    mSuffix = suffix;
}


