#pragma once
#ifndef POINT_H
#define POINT_H

#include <string>


// Exemple d'une classe en C++
// Premier exemple utilisant quelques concepts fondamentaux o� l'encapsulation est mise de l'avant (pas d'h�ritage ou de polymorphisme)


class Point                                                                     //< d�claration de la classe Point
{                                                                               //< d�but du corps de la classe
private:                                                                        //< d�claration d'une section priv�e, cette d�calration est formellement inutile car la premi�re section est priv�e par d�faut, n�anmoins ceci permet de formaliser l'intention du programmeur
    int mX{};                                                                   //< d�calration d'un attribut membre (non statique), avec une initialisation par d�faut � 0
    int mY{};                                                                   //< d�calration d'un attribut membre (non statique), avec une initialisation par d�faut � 0
                                                                                
    static std::string mPrefix, mSeparator, mSuffix;                            //< d�calration de trois attributs membres statiques
                                                                                
public:                                                                         
    Point(int x, int y);                                                        //< d�claration d'un constructeur ayant 2 param�tres (deux entiers pour les x et y)
    Point(int value);                                                           //< d�claration d'un constructeur ayant 1 param�tre (le m�me entier pour les deux valeurs x et y)
    Point();                                                                    //< d�claration d'un constructeur n'ayant aucun param�tre 
                                                                                //    c'est le constructeur par d�faut
                                                                                //    c'est une fonction sp�ciale que le programmeur choisi d'impl�menter lui m�me
                                                                                //    ainsi, l'impl�mentation automatique par le compilateur ne sera pas r�alis�e
    Point(Point const & p) = default;                                           //< d�claration d'un constructeur ayant 1 param�tre (un autre point duquel le point courant sera une copie)
                                                                                //    c'est le constructeur de copie
                                                                                //    c'est une fonction sp�ciale o� le programmeur choisi d'utiliser l'impl�mentation par d�faut
                                                                                //    ainsi, l'impl�mentation automatique par le compilateur sera r�alis�e
    ~Point();                                                                   //< d�claration du destructeur
                                                                                //    c'est une fonction sp�ciale que le programmeur choisi d'impl�menter lui m�me
                                                                                //    ainsi, l'impl�mentation automatique par le compilateur ne sera pas r�alis�e
                                                                                //    ceci est un exemple p�dagogique car l'impl�mentation par d�faut �tait suffisante
                                                                                
    int x() const;                                                              //< accesseur de l'attribut mX                : la fonction ne modifie pas l'objet => const
    int y() const;                                                              //< accesseur de l'attribut mY                : la fonction ne modifie pas l'objet => const
                                                                                
    void setX(int x);                                                           //< mutateur de l'attribut mX                 : la fonction modifie l'objet => la fonction n'est pas const
    void setY(int y);                                                           //< mutateur de l'attribut mY                 : la fonction modifie l'objet => la fonction n'est pas const
    void set(int x, int y);                                                     //< mutateur des attributs mX et mY simultan� : la fonction modifie l'objet => la fonction n'est pas const
                                                                                
    int distance2(Point const & p);                                             //< fonction utilitaire retournant la distance euclidienne au carr� entre deux points (le point courant et le point pass� en param�tre)
    double distance(Point const & p);                                           //< fonction utilitaire retournant la distance euclidienne entre deux points (le point courant et le point pass� en param�tre)
                                                                                
    std::string toString() const;                                               //< fonction utilitaire retournant une cha�ne de caract�res repr�sentant le point
                                                                                
    static std::string prefix();                                                //< accesseur de l'attribut mPrefix            : la fonction �tant statique ne s'applique pas aux objets et => la fonction ne peut �tre const
    static std::string separator();                                             //< accesseur de l'attribut mSeparator         : la fonction �tant statique ne s'applique pas aux objets et => la fonction ne peut �tre const
    static std::string suffix();                                                //< accesseur de l'attribut mSuffix            : la fonction �tant statique ne s'applique pas aux objets et => la fonction ne peut �tre const
    static void setFormat(std::string const & prefix, std::string const & separator, std::string const & suffix);
                                                                                //< mutateur des attributs mPrefix, mSeparator et mSuffix simultan�
                                                                                //                                              : la fonction �tant statique ne s'applique pas aux objets et => la fonction ne peut �tre const
};                                                                              //< fin du corps de la classe (ne pas oublier le ';')

#endif // POINT_H
