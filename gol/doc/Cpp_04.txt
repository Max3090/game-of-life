//      __  _   __                      _             _         _                                       ____            
//    _/_/_| | /_/ _ __ ___   ___ _ __ | |_ ___    __| |_   _  | | __ _ _ __   __ _  __ _  __ _  ___   / ___| _     _   
//   | ____| |/ _ \ '_ ` _ \ / _ \ '_ \| __/ __|  / _` | | | | | |/ _` | '_ \ / _` |/ _` |/ _` |/ _ \ | |   _| |_ _| |_ 
//   |  _|_| |  __/ | | | | |  __/ | | | |_\__ \ | (_| | |_| | | | (_| | | | | (_| | (_| | (_| |  __/ | |__|_   _|_   _|
//   |_____|_|\___|_| |_| |_|\___|_| |_|\__|___/  \__,_|\__,_| |_|\__,_|_| |_|\__, |\__,_|\__, |\___|  \____||_|   |_|   4.0
//                                                                            |___/       |___/                             
//
// Notions � "voir/revoir/formaliser/approfondir" pour ce quatri�me exercice.
//      - Paradigme orient� objet
//          - 3 concepts fondamentaux
//              - encapsulation
//              - h�ritage
//              - polymorphisme
//      - Classe en C++ : encapsulation
//          - D�claration :
//              - une d�claration de classe peut se faire � n'importe quel endroit en C++ (dans un *.cpp ou dans un *.h)
//              - il n'existe pas la contrainte d'une seule classe par fichier ni que le fichier porte le m�me nom
//              - la pratique recommand�e est de faire ce que Java impose (� la sauce du langage C++), c'est � dire :
//                  - on d�clare la classe dans le *.h
//                  - on impl�mente la classe dans le *.cpp
//                  - g�n�ralement, on tente d'avoir une classe par duo de fichier (cpp/h)
//					- en C++, cette recommandation n'est pas absolue et poss�de �videmment plusieurs exceptions
//              - synopsis de la d�claration :
//                  class _nom_classe_
//                  { 
//                      [_type_de_masquage_:]
//                          [_d�claration_variables_]
//                          [_d�claration_fonctions_]
//                  }; // ne pas oublier le point-virgule
//          - Impl�mentation :
//              - l'impl�mentation peut se faire � m�me la d�claration dans le (*.h)
//              - l'impl�mentation se fait g�n�ralement dans le *.cpp
//              - l'impl�mentation d'une fonction doit pr�fixer le nom de la fonction par le nom de la classe et l'op�rateur de port�e (::), par exemple :
//                  int Point::distance(Point const & p) {  // d�clare l'impl�mentation de la fonction distance de la classe Point
//                      ... 
//                  }           
//          - Masquage
//              - Concept.
//              - Types de masquage :
//                  - priv� 		private         // c'est le masquage par d�faut
//                  - prot�g�		protected
//                  - publique		public
//                  - il n'existe pas le masquage 'package' comme en java
//				- Discussion sur l'importance des accesseurs et mutateurs. N�cessaires? Obligatoires?
//          - Constructeur
//              - Concept.
//              - Syntaxe :
//                  - une fonction ayant le m�me nom de la classe
//                  - il peut y avoir n constructeurs (surcharge possible)
//                  - aucun type de retour
//                      class Point {
//                      public:
//                          Point(); 				// d�claration d'un 1er constructeur
//                          Point(int x, int y);	// d�claration d'un 2e constructeur
//                      };
//              - liste d'initialisation membre ('member initializer list')
//                  - concept.
//                  - entre la d�claration du constructeur et du corps de fonction se situe la zone de la liste d'initialisation membre pr�fix�e par les deux-points (:)
//                  - recommand� pour l'initialisation des variables mais absolument n�cessaires pour certains �l�ments :
//                      - d�finition des constantes
//                      - d�finition des r�f�rences
//                      - autres �l�ments qu'on verra plus loin
//                  - exemple :
//                      // Point.h
//                      class Point {
//                          int x, y;
//                      public:
//                          Point(); // d�claration du constructeur
//                      };
//                      // Point.cpp
//                      Point::Point()      // <= d�claration de l'impl�mentation de la fonction constructeur 
//                          : x{}, y{}      // <= zone de la liste d'initialisation membre - on d�termine les valeurs de x et y � l'initialisation, avant l'appel de la fonction du constructeur
//                      {                   // <= d�but du corps de la fonction
//                      }
//              - types de constructeur :
//                  - constructeur par d�faut
//                      - ne poss�de aucun param�tre ou poss�de tous ses param�tres avec une valeur par d�faut
//                          Point();                                // exemple avec aucun param�tre
//                          Point(int x = 0, int y = 0);            // exemple avec deux param�tres ayant des valeurs par d�faut
//																	// note : ces deux constructeurs sont impossibles en m�me temps
//                  - constructeur de copie
//                      - poss�de comme param�tre un objet de m�me nature
//                          Point(Point const & p);                 // on construit un point en copiant un autre point (tr�s utilis� m�me si on ne le voit pas)
//                  - constructeur d�l�gu�
//                      - d�l�gue la construction � un autre constructeur de la m�me classe
//                      - la d�l�gation est d�clar�e � m�me la liste d'initalisation membre
//                          Point(int value_x, int value_y) : x{ valuex }, y{ valuey } {}       // d�claration du construteur avec les deux valeurs x et y
//                          Point() : Point(0, 0) {}                                            // d�claration du constructeur par d�faut d�l�gant la construction au constructeur pr�c�dent
//          - Destructeur
//              - Concept.
//              - Syntaxe :
//                  - une fonction ayant le m�me nom de la classe pr�fix� par le symbole tilde (~)
//                  - il ne peut y avoir qu'un seul destructeur (aucune surcharge possible)
//                  - aucun type de retour
//                      class Point {
//                      public:
//                          Point();    // d�claration du constructeur
//                          ~Point();   // d�claration du destructeur
//                      };
//          - Fonctions membres sp�ciales
//              - il en a 6 (7 d�s C++20)
//              - elles sont sp�ciales dans le sens o� elles peuvent �tre cr��es automatiquement par le compilateur sous certaines conditions
//              - on n'en verra que 4 (3 maintenant - 1 autre plus tard dans la session)
//
//                  | fonction membre sp�ciale  | conditions pour que le compilateur    | que fait l'impl�mentation						|
//                  |                           | en cr�e automatiquement               | par d�faut									|
//                  +---------------------------+---------------------------------------+-----------------------------------------------+
//                  | constructeur par d�faut   | aucun constructeur d�clar�            | rien											|
//                  | constructeur de copie     | non d�clar�                           | copie chaque �l�ment par assignation (a = b;)	|
//                  | destructeur               | non d�clar�                           | rien											|
//
//              - il est possible de formaliser l'intention du d�veloppeur concernant ces fonctions sp�ciales :
//                  - l'utilisation de "... = default" rend explicite la g�n�ration de la version par d�faut
//                  - l'utilisation de "... = delete" rend explicite le fait de ne pas g�n�rer la version par d�faut
//                      class Point {
//                      public:
//                          Point() = default;                      // formalise l'impl�mentation automatique du constructeur par d�faut
//                          Point(Point const & p) = delete;        // rend indisponible le constructeur de copie, aucune impl�mentation automatique
//                                                                  // le destructeur n'est pas d�clar�, une impl�mentation automatique est r�alis�e
//                      };
//          - Fonctions const
//              - il est possible de d�clarer des fonctions membres "en lecture seule" (concept de "immuable" beaucoup utilis� dans le formalisme OO)
//              - ces fonctions peuvent �tre utilis�es par des pointeurs et r�f�rence � des objets "const"
//              - ce concept est tr�s important en C++ et largement utilis�
//              - la d�claration se fait en ajoutant const � la fin de la fonction
//                      class Point {
//                          int x, y;           // d�claration des attributs x et y
//                      public:
//                          int getX() const;   // d�claration de l'accesseur x - en lecture seule
//                      };
//          - Membres statiques
//              - il est pratique de consid�rer ce concept :
//                  - un membre statique est un membre de la classe
//                  - un membre non-statique est un membre d'un objet
//              - variables statiques :
//                  - il existe une seule instance de chaque variable statique pour tout le programme
//                  - �a implique, une seule variable pour tous les objets de cette classe
//              - fonctions statiques :
//                  - une fonction statique peut �tre utilis�e par la classe et par un objet
//                  - toutefois, elle ne peut acc�der qu'� des variables statiques
//              - d�claration : pr�fixer la d�claration standard par le mot r�serv� 'static'
//              - ATTENTION, une variable statique requiert une d�claration d'instance formelle (similaire au formalisme du langage C)
//                      // Fichier Point.h
//                      class Point {
//                          int x, y;
//                          static int w;
//                      public:
//                          void setX(int x);
//                          static void setW(int w);
//                      };
//                      // Fichier Point.cpp
//                      int Point::w{};                                 // <== ATTENTION cette partie est obligatoire!!
//                                                                      // d�clare l'instance formelle de la variable w de la classe Point 
//                                                                      // la variable est initialis�e � 0
//                                                                      // on remarque que le 'static' n'est plus requis ici
//                      void Point::setX(int valueX) { x = valueX; }    // assigne la variable d'objet x � valueX
//                      void Point::setW(int valueW) { w = valueW; }    // assigne la variable de classe w � valueW 
//																		// encore une fois, on remarque que le 'static' n'est plus requis ici
//          - Initialisation des attributs
//              - depuis C++11 il est possible d'initialiser les attributs membres � m�me leur d�claration
//              - il existe certaines r�gles et certaines contraintes (variables statiques ne peuvent �tre initialis�)
//                      class Point {
//                          int x{};            // initialis� � 0
//                          int y{ 10 };        // initialis� � 10
//                      };  
//
//			- 'class' vs 'struct'
//				- en C++, une structure est une classe
//				- il n'y a qu'une seule diff�rence :
//					- dans une classe, le masquage par d�faut est : private
//					- dans une structure, le masquage par d�faut est : public
//				- sinon tout est identique : une structure poss�de au moins un constructeur, un destructeur, des fonctions sp�ciales, ...
//
//			- Un exemple de classe est donn�e en fichier joint : la classe Point (voir les fichiers Point.h et Point.cpp).
//
//
//      - Instances (comparaison avec Java)
//          - en C++, il est possible de d�clarer l'allocation de m�moire statiquement et dynamiquement d'un type fondamental :
//              1   int a{};                    // d�claration d'un entier dont l'allocation est statique (g�r� par le compilateur au 'compile time')
//              2   int * p{ new int };         // d�claration d'un pointeur d'entier dont l'allocation est statique (g�r� par le compilateur au 'compile time')
//              3                               //          toutefois, on assigne � ce pointeur l'adreses d'un entier allou� dynamiquement (g�r� par le programmeur au 'run time')
//              4   delete p;                   // supprime l'entier allou� dynamiquement. N�anmoins, la variable p existe toujours et pointe � une adresse invalide apr�s l'ex�cution de cet op�rateur
//              5   p = nullptr;                // maintenant p est coh�rent avec la pratique des pointeurs
//          - contrairement aux langages interpr�t�s, le C++ permet la m�me chose avec les objets
//              11  string s;                   // d�claration d'un objet de la classe string dont l'allocation est statique (g�r� par le compilateur au 'compile time')
//              12  string * p{ new string };   // d�claration d'un pointeur de 'string' dont l'allocation est statique (g�r� par le compilateur au 'compile time')
//              13                              //          toutefois, on assigne � ce pointeur l'adreses d'un objet 'string' allou� dynamiquement (g�r� par le programmeur au 'run time')
//              14  delete p;                   // supprime le 'string' allou� dynamiquement. N�anmoins, la variable p existe toujours et pointe � une adresse invalide apr�s l'ex�cution de cet op�rateur
//              15  p = nullptr;                // maintenant p est coh�rent avec la pratique des pointeurs
//          - autrement dit, il n'est pas n�cessaire de d�clarer une allocation dynamique avec les objets en C++ comme le requiert Java (voir la ligne 12 qui est �quivalente � ce que vous faites en Java)
//          - on utilise toujours une allocation statique lorqu'elle est possible (voir la ligne 11)
//
//      - D�claration avanc�e - 'Forward declaration'
//          - il est possible d'informer le compilateur qu'un type existe sans le d�finir enti�rement, c'est ce qu'on appel le 'forward declaration'
//          - il suffit de d�clarer le type sans m�me donner le d�tail de sa d�finition :
//              enum my_cool_enum;              // d�claration avanc�e du type �num�r� : on annonce au compilateur que ce type existe : my_cool_enum
//              struct my_cool_struct;          // d�claration avanc�e d'une structure : on annonce au compilateur que ce type existe : my_cool_struct
//              class my_cool_class;            // d�claration avanc�e d'une classe    : on annonce au compilateur que ce type existe : my_cool_class
//          - IMPORTANT :
//              - il est impossible de d�clarer un objet ou l'utilisation d'un objet avec une simple d�claration avanc�e, une d�claration compl�te est requise
//              - il est possible de d�clarer un pointeur ou une r�f�rence d'un objet avec une simple d�claration avanc�e
//              - l'id�e est simple, lorque le compilateur arrive � la d�claration d'un pointeur, il n'a besoin que de r�server 4 octets peut importe la taille du type r�el o� le pointeur pointe
//              - par contre, le compilateur doit r�server enti�rement la m�moire lorsqu'il arrive sur un objet, ainsi, le type doit �tre enti�rement connu (pas l'impl�mentation des fonctions toutefois)
//          - on utilise la d�claration avanc�e dans deux contextes principaux :
//              - d�pendance circulaire de types (diff�rent de la d�pendence circulaire de fichiers)
//              - acc�l�rer le temps de compilation
//          - exemple :
//              // Fichier employe.h
//              class patron;                               // <<< forward declaration ----+
//              class employe                               //                             |
//              {                                           //                             | le #include est impossible, d�pendence circulaire des types
//                  int data1, data2;                       //                             | la seule alternative, c'est le forward declaration qui rend ceci possible
//                  patron * le_patron;                     // (1)                         | dans les deux cas, ce ne sont que des pointeurs et non pas des objets (voir (1) et (2))
//              };                                          //                             |
//              // Fichier patron.h                         //                             |
//              class employe;                              // <<< forward declaration ----+
//              class patron                                //  
//              {                                           //  
//                  int data1, data2;                       //  
//                  vector<employe*> responsable_de;        // (2)
//              };                                          //
//                                                          //
//                                                          // dans cet exemple, on peut supposer que les classes seront utilis�es dans les *.cpp 
//                                                          // ainsi les #include se feront dans ces fichiers respectivement sans probl�me
//
//
//
//