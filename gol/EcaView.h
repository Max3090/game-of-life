#ifndef ECA_VIEW_H
#define ECA_VIEW_H


class EcaModel;                 // forward declaration
#include <string>               // required for : std::string


//////////////////////////////////////////////////////////////////////////
//! \brief Vue de l'automate cellulaire élémentaire.
//!
//! Cette classe réalise l'implémentation de l'affichage de l'automate 
//! cellulaire élémentaire. Elle correspond à la partie _vue_ du patron de 
//! conception MVC. 
//!
//! Elle possède la outils nécessaires à faire l'affichage des messages de 
//! l'application principale et du modèle.
//////////////////////////////////////////////////////////////////////////
class EcaView
{
public:
    //////////////////////////////////////////////////////////////////////////
    //! \brief Constructeur par défaut.
    //////////////////////////////////////////////////////////////////////////  
	EcaView() = default;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Destructeur par défaut.
    //////////////////////////////////////////////////////////////////////////  
    ~EcaView() = default;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Affiche un message d'intéraction avec l'usager sur la console.
    //!
    //! \param toUser le message destiné à l'utilsiateur à afficher
    //! \param clearScreen si vrai, efface l'écran avant l'affichage du 
    //! message
    //////////////////////////////////////////////////////////////////////////  
    void show(std::string const & toUser, bool clearScreen = true);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Affiche le modèle.
    //!
    //! \param model le modèle à afficher
    //////////////////////////////////////////////////////////////////////////  
    void show(EcaModel const & model);

private:
	std::string getHorizontalAxis(size_t from, size_t to, size_t offsetCharCount, char offsetChar = ' ');
};


#endif // ECA_VIEW_H