#include "State.h"


#include "RandomUtil.h"		// required for : RandomUtil::generateEvent 


char State::sInactiveRepresentation{ ' ' };
char State::sActiveRepresentation{ '*' };


State::State(Value value)
	: mValue{ value }
{
}

State::Value State::value() const
{
	return mValue;
}

void State::setValue(Value value)
{
	mValue = value;
}

void State::randomize(double probability)
{
	mValue = (Value)RandomUtil::generateEvent(probability);
}

void State::toggle()
{
	mValue = (Value)!(bool)mValue;
}

State State::toggled() const
{
	return State((Value)!(bool)mValue);
}

void State::setRepresentation(char inactive, char active)
{
	sInactiveRepresentation = inactive;
	sActiveRepresentation = active;
}

char State::toChar() const
{
	return (bool)mValue ? sActiveRepresentation : sInactiveRepresentation;
}
