#ifndef ECA_CONTROLLER_H
#define ECA_CONTROLLER_H


#include <array>
#include "EcaModel.h"
#include "EcaView.h"

#include "EcaActionKeyBinding.h"


//////////////////////////////////////////////////////////////////////////
//! \brief Contrôleur de l'application.
//!
//! Cette classe réalise la gestion principale de l'application. Elle 
//! correspond à la partie _contrôleur_ du patron de conception MVC. 
//!
//! D'abord, le contrôleur possède deux références vers les parties 
//! _modèle_ et _vue_ de la partie MVC. En fait, l'instanciation d'un 
//! contrôleur n'est possible que si des objets EcaModel et EcaView lui 
//! sont passé en paramètre. Ces instances sont passées en référence et 
//! leur durée de vie doivent être au minimum la même que celle du 
//! contrôleur. Le contrôleur n'a pas possession de ces objets.
//!
//! Le contrôleur possède une instance de la classe EcaActionKeyBinding 
//! et lui permet d'associer simplement et efficacement les tâches liées 
//! aux touches du clavier.
//!
//! Son interface de prommation est très simple. Toute l'exécution du 
//! programme est masquée à l'utilisateur. Il suffit d'appeler la 
//! fonction `start` pour exécuter le programme.
//////////////////////////////////////////////////////////////////////////
class EcaController
{
public:
    //////////////////////////////////////////////////////////////////////////
    //! \brief Constructeur avec valeurs d'initialisation.
    //!
    //! \param model l'instance du modèle
    //! \param view l'instance de la vue
    //////////////////////////////////////////////////////////////////////////   
	EcaController(EcaModel & model, EcaView & view);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Destructeur par défaut.
    //////////////////////////////////////////////////////////////////////////   
    ~EcaController() = default;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Démarre l'application.
    //!
    //! Effectue la gestion complète de l'application. Gère :
    //!     - le démarrage du projet
    //!     - les intéractions avec l'usager (affichage et saisie des instructions) 
    //!     - gestion du modèle (simulation)
    //////////////////////////////////////////////////////////////////////////   
    void start();

private:
	bool mQuit = false;
	EcaModel & mModel;
	EcaView & mView;

	enum class GenerationMode {
		Centered_1_2	= 0,
		Centered_1_4	= 1,
		Centered_1_8	= 2,
		Centered_1_16	= 3,
		Random25		= 4,
		Random50		= 5,
		Random75		= 6,
		Checked0		= 7,
		Checked1		= 8,
		_count_
	};
	GenerationMode mGenerationMode = GenerationMode::Random50;
	EcaActionKeyBinding mGenerateAction;

	enum class KeyBinding : char {
		Action_Quit				= 27,	// escape character
		Action_Prev100			= '1',
		Action_Prev10			= '2',
		Action_Prev5			= '3',
		Action_Prev2			= '4',
		Action_Prev1			= '5',
		Action_Same				= ' ',
		Action_Next_1			= '6',
		Action_Next_2			= '7',
		Action_Next_5			= '8',
		Action_Next_10			= '9',
		Action_Next_100			= '0',
		Action_PrevNotable		= 'N',
		Action_NextNotable		= 'M',
		GenMode_Centered1_2		= 'Z', 
		GenMode_Centered1_4		= 'X',
		GenMode_Centered1_8		= 'C',
		GenMode_Centered1_16	= 'V',
		GenMode_Random25		= 'A',
		GenMode_Random50		= 'S',
		GenMode_Random75		= 'D',
		GenMode_Checked0		= 'Q',
		GenMode_Checked1		= 'W',
		_count_
	};
	EcaActionKeyBinding mModelAction;
	
	void showIntroduction();
	void setup();
	void generate();
	void quit();

	static std::string sWelcomeText;
	static std::string sSpaceSizeText;
	static std::string sTimeSizeText;
	static std::string sIsQuittingText;
	static std::string sQuitInterruptionText;
	static char sQuitConfirmationCharacter;
};




#endif // ECA_CONTROLLER_H