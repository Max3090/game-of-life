// ...
// ...
// ...

#ifndef CONSOLE_UTIL_H
#define CONSOLE_UTIL_H


#include <string>


//////////////////////////////////////////////////////////////////////////
//! \brief Classe consolidant plusieurs fonctions 
//! utilitaires liées à l'utilisation et l'interaction avec la console.
//! 
//! Les fonctions sont statiques et ne requièrent pas d'instance de la 
//! classe.
//////////////////////////////////////////////////////////////////////////
class ConsoleUtil
{
public:
	//////////////////////////////////////////////////////////////////////////
	//! \brief Le constructeur est retiré puisqu'aucune instance de 
	//! cette classe n'est attendue.
	//////////////////////////////////////////////////////////////////////////
	ConsoleUtil() = delete;

	//////////////////////////////////////////////////////////////////////////
	//! \brief Le destructeur est retiré puisqu'aucune instance de 
	//! cette classe n'est attendue.
	//////////////////////////////////////////////////////////////////////////
	~ConsoleUtil() = delete;

	//////////////////////////////////////////////////////////////////////////
	//! \brief Vide l'écran.
	//////////////////////////////////////////////////////////////////////////
	static void clearScreen();

	//////////////////////////////////////////////////////////////////////////
	//! \brief Lis le prochain caractère saisi et retourne sa valeur en 
	//! majuscule.
	//!
	//! \return retourne le caractère saisi mis en majuscule
	//////////////////////////////////////////////////////////////////////////	
	static char getUpperChar();

	//////////////////////////////////////////////////////////////////////////
	//! \brief Valide si le caractère saisi correspond à un caractère attendu.
	//!
	//! Cette fonction est insensible à la casse.
	//!
	//! \param[in] capChar est le caractère attendu
	//! \return vrai si le caractère saisi est celui attendu, retourne  faux 
	//! sinon
	//////////////////////////////////////////////////////////////////////////	
	static bool validateUpperChar(char capChar);

	//////////////////////////////////////////////////////////////////////////
	//! \brief Demande à l'usager d'appuyer sur une touche avant de 
	//! poursuivre.
	//////////////////////////////////////////////////////////////////////////	
	static void pressAnyKey();

	//////////////////////////////////////////////////////////////////////////
	//! \brief Retourne un entier saisi au clavier.
	//!
	//! Cette fonction affiche d'abord le titre d'invite et attend que 
	//! l'usager appuie sur `enter`. Si le texte saisie correspond 
	//! complètement à un entier, la valeur numérique est retournée. Si la 
	//! chaîne de caractères n'est pas conforme, la fonction recommence tant 
	//! que la valeur saisie ne soit pas valide.
	//!
	//! \param[in] title l'invite indiquant le contexte de la saisie
	//! \return retourne le nombre entier correspondant à la valeur saisie.
	//////////////////////////////////////////////////////////////////////////	
	static int getInt(std::string const & title);

	//////////////////////////////////////////////////////////////////////////
	//! \brief Retourne un entier borné saisi au clavier.
	//!
	//! Cette fonction affiche d'abord le titre d'invite et attend que 
	//! l'usager appuie sur `enter`. Si le texte saisie correspond 
	//! complètement à un entier et est inclus dans les bornes spécifiées, 
	//! la valeur numérique est retournée. Si la chaîne de caractères n'est 
	//! pas conforme ou si l'entier est à l'extérieur des bornes, la fonction 
	//! recommence tant que la valeur saisie ne soit pas valide.
	//!
	//! \param[in] title l'invite indiquant le contexte de la saisie
	//! \param[in] min borne inférieure de l'interval
	//! \param[in] max borne supérieure de l'interval
	//! \param[in] addLimitsToTitle indique que les bornes sont ajoutés à 
	//! l'invite
	//! \return retourne le nombre entier correspondant à la valeur saisie.
	//////////////////////////////////////////////////////////////////////////	
	static int getInt(std::string title, int min, int max, bool addLimitsToTitle = true);

private:
	//////////////////////////////////////////////////////////////////////////
	// isStringInt return true if all character meet the integer literal 
	// requirements.
	// La fonction std::stoi retourne un entier même si la fin de la chaîne 
	// de caractères est invalide (en autant qu'il y ait un int au début).
	//////////////////////////////////////////////////////////////////////////
	static bool isStrictInt(std::string const & str);
};


#endif // CONSOLE_UTIL_H