#ifndef ECA_MODEL_H
#define ECA_MODEL_H


#include "SpaceTime.h"              // required for : SpaceTime 
#include "Rule.h"                   // required for : Rule 


//////////////////////////////////////////////////////////////////////////
//! \brief Modèle de l'automate cellulaire élémentaire.
//!
//! Cette classe réalise l'implémentation des données et des traitements 
//! fondamentaux de l'automate cellulaire élémentaire. Elle correspond à 
//! la partie _modèle_ du patron de conception MVC. 
//!
//! Elle possède la représentation de l'espace temps (`SpaceTime`), de la 
//! règle (`Rule`) et des fonctions les manipulant dans un tout cohérent 
//! et facile d'utilisation.
//!
//! Sont disponibles des accesseurs sur la taille de l'espace et le temps 
//! ainsi qu'un mutateur pour les déterminer.
//!
//! Plusieurs fonctions permettent de générer la simulation selon divers 
//! outils d'initialisation :
//!     - initialisation uniforme selon un état
//!     - initialisation aléatoire uniforme avec un biais
//!     - initialisation selon _n_ partitions de l'espace
//!     - initialisation selon un patron de type damier
//!
//! Des fonctions permettant de déterminer la règle :
//!     - une règle spécifique
//!     - la _n<sup>e</sup>_ règle suivante
//!     - la _n<sup>e</sup>_ règle précédente
//!     - la règle d'intérêt suivante
//!     - la règle d'intérêt précédente
//!
//! Deux fonctions utilitaires permettent de transformer la règle et 
//! l'espace temps en chaîne de caractères afin de faciliter leur 
//! représentation.
//////////////////////////////////////////////////////////////////////////
class EcaModel
{
public:
    //////////////////////////////////////////////////////////////////////////
	//! \brief Constructeur par défaut initialisant entièrement le modèle à 
    //! vide. 
    //! 
    //! L'espace temps n'existe pas (de taille 0) et la règle est initialisée 
    //! à 0.
	//////////////////////////////////////////////////////////////////////////
	EcaModel() = default;
	
    //////////////////////////////////////////////////////////////////////////
    //! \brief Destructeur par défaut.
    //////////////////////////////////////////////////////////////////////////    
    ~EcaModel() = default;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Redimensionne l'espace temps.
    //!
    //! \param spaceSize la taille de l'univers
    //! \param timeSize la taille du temps
    //////////////////////////////////////////////////////////////////////////   
	void resize(size_t spaceSize, size_t timeSize);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Accesseur retournant la taille de l'espace.
    //!
    //! \return la taille de l'espace
    //////////////////////////////////////////////////////////////////////////   
    size_t spaceSize() const;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Accesseur retournant la taille du temps.
    //!
    //! \return la taille du temps
    //////////////////////////////////////////////////////////////////////////   
    size_t timeSize() const;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Génère une simulation complète avec une initialisation 
    //! uniforme.
    //!
    //! L'initialisation met la même valeur d'état pour toutes les cellules 
    //! au temps 0.
    //!
    //! \param state l'état initiale de toutes les cellules
    //////////////////////////////////////////////////////////////////////////   
	void generateFromState(State const & state);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Génère une simulation complète avec une initialisation 
    //! uniforme.
    //!
    //! L'initialisation génère les états initiaux aléatoirement selon un 
    //! certain biais.
    //!
    //! \param probability donne le pourcentage du biais vers un état actif 
    //! (`State::Value::Active`)
    //////////////////////////////////////////////////////////////////////////   
    void generateFromRandom(double probability = 0.5);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Génère une simulation complète selon une initialisation 
    //! où l'espace est compartimenté.
    //!
    //! Toute l'espace est inactif sauf les _n_ cellule séparant les zones 
    //! de séparation qui sont actives.
    //!
    //! \param split le nombre de compartimentation à faire
    //////////////////////////////////////////////////////////////////////////   
    void generateFromCentered(size_t split = 2);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Génère une simulation complète avec une initialisation 
    //! selon un patron de damier.
    //!
    //! L'initialisation met chacune des cellules en alternance. 
    //!
    //! \param state donne la valeur de départ de la séquence du damier
    //////////////////////////////////////////////////////////////////////////   
    void generateFromChecked(State const & state = State());

    //////////////////////////////////////////////////////////////////////////
    //! \brief Détermine la règle à une valeur spécifique.
    //!
    //! \param rule la valeur de la règle
    //////////////////////////////////////////////////////////////////////////   
	void setRule(Rule::value_t rule);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Détermine la règle à la _i<sup>e</sup>_ valeur suivante.
    //!
    //! La règle est considérée dans un espace circulaire.
    //!
    //! \param i la valeur d'incrément à réaliser
    //////////////////////////////////////////////////////////////////////////   
    void nextRule(size_t i = 1);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Détermine la règle à la _i<sup>e</sup>_ valeur précédente.
    //!
    //! La règle est considérée dans un espace circulaire.
    //!
    //! \param i la valeur de décrémente à réaliser
    //////////////////////////////////////////////////////////////////////////   
    void previousRule(size_t i = 1);

    //////////////////////////////////////////////////////////////////////////
    //! \brief Détermine la règle à la prochaine règle d'intérêt.
    //!
    //! La règle est considérée dans un espace circulaire.
    //////////////////////////////////////////////////////////////////////////   
    void nextNotableRule();

    //////////////////////////////////////////////////////////////////////////
    //! \brief Détermine la règle à la règle d'intérêt précédente.
    //!
    //! La règle est considérée dans un espace circulaire.
    //////////////////////////////////////////////////////////////////////////   
    void prevNotableRule();

    //////////////////////////////////////////////////////////////////////////
    //! \brief Retourne une chaîne de caractères représentant l'espace temps.
    //!
    //! L'axe horizontal représente l'espace et l'axe verticale représente 
    //! le temps.
    //!
    //! Cette fonction, utilise la représentation déterminée dans la classe 
    //! `State`. Voir la fonction State::setRepresentation pour déterminer la 
    //! représentation des états.
    //////////////////////////////////////////////////////////////////////////   
    std::string spaceTimeToString() const;

    //////////////////////////////////////////////////////////////////////////
    //! \brief Retourne une chaîne de caractères représentant la règle.
    //!
    //! \param[in] includeBinaryRepresentation inclus la représentation 
    //! binaire de la règle
    std::string ruleToString(bool includeBinaryRepresentation = true) const;

private:
	SpaceTime mSpaceTime;
	Rule mRule;

	void applyRule(size_t space, size_t time);
	void applyRule(size_t time);
	void applyRule();
};


#endif // ECA_MODEL_H
