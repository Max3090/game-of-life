#include "EcaView.h"


#include "EcaModel.h"
#include "ConsoleUtil.h"
#include <iostream>
#include <sstream>


void EcaView::show(std::string const & toUser, bool clearScreen)
{
	if (clearScreen) {
		ConsoleUtil::clearScreen();
	}

	std::cout << toUser;
}

void EcaView::show(EcaModel const & model)
{
	size_t const offsetCharCount{ 5 };
    char const offsetChar{ ' ' };
	std::string separator((int)model.spaceSize() - offsetCharCount, '-');
	separator += '\n';

	ConsoleUtil::clearScreen();
	std::cout	<< separator
				<< model.ruleToString(true)
				<< separator
                << getHorizontalAxis(0, model.spaceSize() - 1, offsetCharCount, offsetChar)
				<< model.spaceTimeToString();
}


// Fonction peu performante mais donne un exemple intéressant sur l'usage des itérateurs et des lambdas
std::string EcaView::getHorizontalAxis(size_t from, size_t to, size_t offsetCharCount, char offsetChar)
{
    size_t nDigitsRequired{ (size_t)std::floor(std::log10(to)) + 1ull };
    size_t lineLength{ to - from + 1 + offsetCharCount + 1 };
    size_t strlength{ lineLength * nDigitsRequired };

    std::string str(strlength, offsetChar); // '\0' is implicitely appended at the end

    using string_iterator = std::string::iterator;
    auto assignChar{ [&str, from, offsetCharCount, nDigitsRequired, lineLength](string_iterator const & it)->void {
            size_t pos{ static_cast<size_t>(std::distance(str.begin(), it)) };
            size_t pow10Order{ pos / lineLength };
            size_t digitOrder{ pos % lineLength };
            if (digitOrder == lineLength - 1) {
                *it = '\n';
            } else if (digitOrder > offsetCharCount) {
                *it = '0' + (char)((digitOrder - offsetCharCount + from) / static_cast<size_t>(std::pow(size_t{ 10 }, nDigitsRequired - pow10Order - 1)) % size_t { 10 });
            }// else {                          // implicit in the algorithm
             //     *curChar = offsetChar;      // already done
             // }
        } };

    string_iterator curChar{ str.begin() };
    string_iterator endChar{ str.end() };
    while (curChar != endChar) {
        assignChar(curChar);
        ++curChar;
    }

    return str;
}
