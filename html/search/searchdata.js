var indexSectionsWithContent =
{
  0: "abcdefgiklmnprstvw~",
  1: "bcemrs",
  2: "abcdefgilmnprstvw~",
  3: "akv",
  4: "v",
  5: "ai",
  6: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "typedefs",
  4: "enums",
  5: "enumvalues",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Functions",
  3: "Typedefs",
  4: "Enumerations",
  5: "Enumerator",
  6: "Pages"
};

